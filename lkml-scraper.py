#!/usr/bin/env python3
import argparse
from datetime import datetime, timedelta
import os # getpid(), path.*()
import random # uniform()
import requests
import signal
import socket # gethostname()
import time # time(), sleep()
from bs4 import BeautifulSoup
from itertools import permutations, islice

# Global variable for command line arguments
CLARGS = {}
# global variable for sigterm handler
STOPPED = False

def handle_sigterm(signum, frame):
    global STOPPED
    print('Received signal to stop. Waiting for current download to finish, \
    Then I will terminate.')
    STOPPED = True

# Scrapes the given url for properties of the mail and returns an object with
# them.
# The returned object is guaranteed to contain the following properties:
# "url": The url which was scraped
# "err": True if an error occured, False otherwise
# "err_msg": A string containing more information about an error
# The following properties are only guaranteed to be available if "err" is False
# "date": The date when the mail was sent
# "from": The author of the mail
# "subject": The subject of the mail
# "body": The body of the mail
def get_mail(url):
    # stores all properties we find
    mail = {
        "url": url,
        "err": True,
        "err_msg": "An internal error occured. Please report this as a bug!"
    }
    # date, sender, subject, and body
    MAIL_PROPERTIES_EXPECTED = 4;
    mail_properties_found = 0;

    # make get request
    r = requests.get(url);
    if r.status_code != 200:
        return {
            "url": url,
            "err": True,
            "err_msg": "Server returned status code: " + str(r.status_code)
        }

    # parse HTML via lxml
    soup = BeautifulSoup(r.text, 'lxml')

    # find and extract meta-data (date, sender, and subject)
    for element in soup.find_all(attrs = {"class": "rp"}):
        itemprop = element.get('itemprop')
        if itemprop == "datePublished":
            mail['date'] = element.get_text()
            mail_properties_found += 1
        elif itemprop == "name":
            mail['subject'] = element.get_text()
            mail_properties_found += 1
        elif itemprop == "author":
            mail['from'] = element.get_text()
            mail_properties_found += 1

    # find and extract mail-body
    mailBody = soup.find("pre", attrs = {"itemprop": "articleBody"})
    if mailBody:
        mail['body'] = mailBody.get_text(separator=u"\n")
        mail_properties_found += 1

    # check whether we found everything
    if mail_properties_found == MAIL_PROPERTIES_EXPECTED:
        mail['err'] = False
        mail['err_msg'] = "No error!"
    else:
        mail['err_msg'] = "Could not retrieve all properties."

    return mail

# Returns the nth item of an unsubscriptable iterable, or a default value
# See http://stackoverflow.com/a/12007859
def nth(iterable, n, default=None):
    return next(islice(iterable, n, None), default)

# Fetches the amount of mails available per day in the given month
# Returns an array of numbers
# index 0 corresponds to the first day of the month
# index 1 corresponds to the second day of the month
# and so on
def fetch_mail_amount_for_month(year, month):
    url = 'https://lkml.org/lkml/' + str(year) + '/' + str(month)
    r = requests.get(url);
    if r.status_code != 200:
        raise ValueError('Server returned status code: ' + str(r.status_code))

    # parse HTML via lxml
    soup = BeautifulSoup(r.text, 'lxml')

    table = soup.find_all(attrs={"class": "mh"})[0]
    # index 0 = unused, then 31 days
    mail_amount = [0] * 32
    for row in table.children:
        try:
            td = nth(row.children, 1)
            a = nth(td.children, 0)
            # some days don't have any mails, therefore we need to check the
            # specific day of this entry via the href-attribute of the link
            day = int(a['href'].split('/')[-1])
            value = a.get_text()
            mail_amount[day] = int(value)
        except AttributeError:
            # in case of an error, set a default value
            print('WARNING: Could not parse mail amount from:', row)

    return mail_amount


# Generator which yields a unique file name for a mail to be saved in
# Each file name is of the form:
# ts.pid_num.host,S
# where:
# ts = current unix time stamp
# pid = process id of this instance
# num = number of mail in this instance
# host = hostname
# everything else is a literal.
# eg: 1485528391.9633_17.vimes,S
# !! Note: Within the program, don't call this generator directly but
# use the global FILENAME variable below
# I don't know how to design this in a better way..
def __generator_get_filename():
    # initialize values that will never change
    pid = str(os.getpid())
    # replace bad chars of hostname, see https://cr.yp.to/proto/maildir.html
    # for more info
    hostname = socket.gethostname() \
                    .replace(' ', '').replace('/', '\057').replace(':', '\072')
    # initialize counter
    num = 0

    while True:
        num += 1
        yield str(int(time.time())) \
            + "." + str(pid) \
            + "_" + str(num) \
            + "." + hostname

# 'Hides' the generator, call next(FILENAME) each time you need a new filename
FILENAME = __generator_get_filename()

# Saves a mail in a new maildir-compatible file with a unique file name.
# The passed mail-Object is expected to contain at least the following
# properties:
# "date", "from", "subject", "url", "body"
# Returns: a boolean, signalizing whether the mail was successfully saved
def save_mail(mail):
    global CLARGS
    if 'date' in mail \
            and 'from' in mail \
            and 'subject' in mail \
            and 'body' in mail \
            and 'url' in mail:
        fname = next(FILENAME)

        # omitting a try-catch block here because we assume that the caller
        # checked the maildir for validity. If no we'll just forward any
        # exceptions
        tmp_fname = CLARGS.maildir + '/tmp/' + fname
        with open(tmp_fname, "w") as f:
            # headers extracted from lkml.org
            f.write("Date: " + mail['date'])
            f.write("\nFrom: " + mail['from'])
            f.write("\nSubject: " + mail['subject'])
            # headers which are always the same when you're subscrubed to the
            # mailing list
            f.write("\nSender: linux-kernel-owner@vger.kernel.org")
            f.write("\nPrecedence: bulk")
            f.write("\nList-ID: <linux-kernel.vger.kernel.org>")
            f.write("\nX-Mailing-List: linux-kernel@vger.kernel.org")
            # Add a custom header with the respective lkml-url for debugging
            # purpose and if one wants to download attachements
            f.write("\nX-Lkml-Link: " + mail['url'])
            # finally, insert an empty line, followed by the body of the mail
            f.write("\n\n" + mail['body'])

        # writting was successful, so we just need to move the mail to the
        # /cur directory.
        cur_fname = CLARGS.maildir + '/cur/' + fname + ":2,S"
        os.rename(tmp_fname, cur_fname)

        # finally, return true
        return True

    return False

# Validates whether the specified arguments are valid enough for the program
# to work without errors. The performed checks include:
# * is the specified maildir really a maildir?
# * do we have permissions to write there?
# The function returns a tuple of (bool, string) where the bool indicates
# whether the given arguments are valid and string contains a detailed error
# message in case of invalid arguments
def validate_args(args):
    # check if maildir is a valid maildir
    check = os.path.isdir(args.maildir)
    check = check and os.path.isdir(args.maildir + '/tmp')
    check = check and os.path.isdir(args.maildir + '/new')
    check = check and os.path.isdir(args.maildir + '/cur')
    if not check:
        msg = '\"' + args.maildir + '\" is not a valid maildir!'
        return (False, msg)

    # check if maildir is writeable
    check = check and os.access(args.maildir + '/tmp', os.W_OK)
    check = check and os.access(args.maildir + '/cur', os.W_OK)
    if not check:
        msg = 'No write-permissions for \"' + args.maildir + '\"!'
        return (False, msg)

    check = check and args.first_mail_id > 0
    if not check:
        msg = str(args.first_mail_id) + ' is an invalid mail id!'
        return (False, msg)

    return (check, '')

# Returns a generator which yields urls to each mail within the given timespan
# Raises ValueError if the given dates are invalid.
def lkml_url_generator(startdate, enddate, first_id=1):
    check = False
    # check if dates are valid
    today = datetime.today()
    try:
        start = datetime.strptime(startdate, "%Y-%m-%d")
    except ValueError:
        check = False
    else:
        check = start <= today
    if not check:
        raise ValueError('Start date is invalid!')

    try:
        end = datetime.strptime(enddate, "%Y-%m-%d")
    except ValueError:
        check = False
    else:
        check = end <= today
        check = check and start <= end
    if not check:
        raise ValueError('End date is invalid!')

    # generator logic starts here
    day = start
    month = {'number': -1, 'mail_amount': []}
    while(day <= end):
        # first check if we entered a new month. if we did, we need to fetch
        # the number of mails for each day
        if day.month != month['number']:
            month['mail_amount'] = \
                fetch_mail_amount_for_month(day.year, day.month)
            month['number'] = day.month

        # yield urls to all mails for that day
        for mail in range(first_id, month['mail_amount'][ day.day ] + 1):
            yield 'https://lkml.org/lkml/' \
                + str(day.year) + "/" \
                + str(day.month) + "/" \
                + str(day.day) + "/" \
                + str(mail)

        # Reset first_id, so that for day 2 and following we start at the first
        # mail for that day
        first_id = 1

        # go to the next day
        day += timedelta(days=1)

# Sleeps for a random interval (between 1 and 2 seconds)
def random_sleep():
    amount = random.uniform(1.0, 2.0)
    time.sleep(amount)

def main():
    global CLARGS
    global STOPPED

    # Register signal handlers
    signal.signal(signal.SIGINT, handle_sigterm)
    signal.signal(signal.SIGTERM, handle_sigterm)

    # parse command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('maildir', type=str,
        help='Path of the maildir-directory where mails should be saved')
    parser.add_argument('--start-date', type=str, required=True,
        help='Download mails of this day and later. \
        Must be in ISO 6801 format (ie. \"2017-01-20\"). \
        Must not be in the future.')
    parser.add_argument('--end-date', type=str, required=True,
        help='Download mails until this day (inclusive). \
        Must be in ISO 6801 format (ie. \"2017-01-20\"). \
        Must not be in the future. \
        Must not be earlier than \'--start-date\' \
        Note: If \'--start-date\' and \'--end-date\' are the same, all mails \
        of the given date are downloaded. \
        Note: The tool will NOT check if any mails were already downloaded by \
        an earlier execution of the tool. It is up to the user to ensure to \
        avoid such duplicate downloads.')
    parser.add_argument('--first-mail-id', type=int, required=False, default=1,
        help='First mail id which will be downloaded from day specified by \
        \'--start-date\'. Must be larger than 0. If it is an invalid number \
        (ie. larger than the highest id for that day) it will be silently \
        ignored and no mails for that day will be downloaded. Beginning from \
        the day afterwards, all mails for each day will be downloaded. \
        This option is mainly intended for continuing previous downloads which \
        were interrupted.')
    args = parser.parse_args()

    (valid, msg) = validate_args(args)
    if valid:
        CLARGS = args

        urls = lkml_url_generator(args.start_date,
            args.end_date,
            args.first_mail_id)
        count_success = 0
        count_error = 0
        for url in urls:
            # first and most important: sleep for a random interval, to reduce
            # traffic/spam
            random_sleep()
            if STOPPED:
                print('Terminated')
                break

            mail = get_mail(url)
            if mail['err']:
                print("Failed to download mail from \"", url,
                    "\":\n", mail['err_msg'])
                count_error += 1
            else:
                check = save_mail(mail)
                if check:
                    count_success += 1
                else:
                    print('Failed to save mail! (', url,')')
                    count_error += 1

            if STOPPED:
                print('Terminated')
                break
    else:
        print("Inavlid arguments provided: ", msg)

    print('Statistics:', count_success, 'successful and',
        count_error, 'failed downloads.')

if __name__ == '__main__':
    main()
